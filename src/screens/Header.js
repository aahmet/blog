/*
** Navbar tasarımı
*/

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import  { Redirect } from 'react-router-dom';
import ReactDOM from 'react-dom';
var data = require('../store/data');

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: false
        };

        // bind methods
        this.updateDimensions = this.updateDimensions.bind(this);
    }

    static contextTypes = {
        router: PropTypes.object
    }
    
    componentWillMount(){
        window.addEventListener("resize", this.updateDimensions);
        this.setNavbarHeight();
    }

    componentDidMount(){
        window.addEventListener("resize", this.updateDimensions);
        this.setNavbarHeight();
    }
    
    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }

    updateDimensions(){
        this.setNavbarHeight();    
    }

    // navbar yüksekliğini global değere atıyoruz
    setNavbarHeight(){
        data.navbarHeight = this._navbar ? ReactDOM.findDOMNode(this._navbar).offsetHeight : 51;
    }

    // router
    setRedirect() {
        this.setState({
            redirect: true
        });
    }

    // router
    renderRedirect() {
        if (this.state.redirect) {
          return <Redirect to='/#iletisim'/>
        }
    }

    // router
    redirectToTarget(){
        this.props.history.push('/#iletisim')
    }

    render() {
        return (
            <div>
                {this.renderRedirect()}
                <Navbar 
                    ref={(e) => this._navbar = e}
                    fixedTop={true} 
                    inverse={true} 
                    onSelect={(e)=>{
                        this.setNavbarHeight();
                        if (e === 1 || e === 2 ) {
                            //this.setRedirect();
                            //this.redirectToTarget();
                            //console.log("prms"+JSON.stringify(Object.keys(window), null, 2));
                            // console.log("innerWidth "+window.innerWidth);
                            // console.log("innerHeight "+window.innerHeight);
                            console.log("scrollY "+window.scrollY);
                            console.log("pageYOffset "+window.pageYOffset);
                            console.log("screenY  "+window.screenY);
                            setTimeout(() => {
                                console.log("prm "+(window.pageYOffset - data.navbarHeight));
                                // window.scrollTo(0,window.pageYOffset - data.navbarHeight);
                                window.scrollTo({
                                    top: window.pageYOffset - data.navbarHeight, 
                                    behavior: 'smooth',
                                });
                            }, 50);
                            
                        }
                    }}>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <a href="">Ana Sayfa DENEME</a>
                        </Navbar.Brand>
                    </Navbar.Header>
                    <Nav>
                        <NavItem eventKey={1} href="#contents">Blog</NavItem>
                        <NavItem eventKey={2} href="#iletisim">İletişim</NavItem>
                        <NavDropdown eventKey={3} title="Diğer" id="basic-nav-dropdown">
                            <MenuItem eventKey={3.1} href="">Hakkımda</MenuItem>
                            <MenuItem eventKey={3.2}>CV</MenuItem>
                            <MenuItem eventKey={3.3}>Referanslar</MenuItem>
                            <MenuItem divider />
                            <MenuItem eventKey={3.4}>GitLab</MenuItem>
                        </NavDropdown>
                    </Nav>
                </Navbar>
            </div>
        );
    }
}

export default Header;
