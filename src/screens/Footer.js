/*
** Footer kısmı
*/

import React, {Component} from 'react';
import './../App.css';
import { Row,Col } from 'react-bootstrap';

export default class Footer extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <div className="container">
                <Row className="show-grid">
                    <Col xs={12} sm={6} md={4}>
                        <h3>PRODUCTS</h3>
                        <div>
                            <div>
                                Mobile Apps
                            </div>
                            <div>
                                Websites
                            </div>
                            <div>
                                Designs
                            </div>
                            <div>
                                Project Management
                            </div>
                        </div>
                    </Col>
                    <Col xs={12} sm={6} md={4}>
                        <h3>BLOG PROJECT</h3>
                        <div>
                            <div>
                                About
                            </div>
                            <div>
                                Privacy Policy
                            </div>
                            <div>
                                FAQ
                            </div>
                            <div>
                                Contact Us
                            </div>
                        </div>
                    </Col>
                    <Col xs={12} sm={6} md={4}>
                        <h3>CONTECT WITH US</h3>
                        <div>
                            <div>
                                <a>Facebook</a>
                            </div>
                            <div>
                                Twitter
                            </div>
                            <div>
                                LinkedIn
                            </div>
                            <div>
                                Instagram
                            </div>
                        </div>
                    </Col>
                </Row>

                <Row className="show-grid">
                    <h5 className="text-center">Tüm hakkı Candy'ne aittir. </h5>
                </Row>
            </div>
        );
    }
}