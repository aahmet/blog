/*
** Blog postlarının listelendiği kısım
*/

import React, {Component } from 'react';
import {Pager, Row, Col, Button, Modal} from "react-bootstrap";
import './../App.css';

var blogList = [
  {
    title: "Kurumsal Kimlik Çalışması Nasıl Yapılır?",
    content: "Genel olarak çoğu insan tanınmak ve bilinmek ister. İnsanlar bunu isterken yaptıkları işin de tanınıp diğer herkesten ayrı yerlerde tutulmasının hayalini kurar. Peki sadece hayalde bırakmak bu düşünceyi ne kadar doğru? Eğer belirli bir yere gelememiş ve kendinizi duyuramamışsanız bunda sizin henüz bu iş için çalışmaya geçmemeniz etkilidir.Bir firma, işletme gibi yere sahipseniz adınızı duyurmak için ilk olarak yapmanız gereken şey insanların aklında kalıcı bir şekilde kurumsal kimlik oluşturmanız gerekmektedir. Kurumsal kimlik işte böyle durumlarda kullanılan ve çoğu firmanın da adını duyurmasını sağlayan etkendir. Bizler bir işe başlamadan önce ilk olarak çalışacağımız kurumu, içeriğini araştırır ve ona göre hareket ederiz. Kurumsal kimlik de böyle durumlarda firmalar hakkında bilgi edinmemizi ve onlar hakkında ön düşünceye sahip olmamızı sağlar. Firmalar kendilerini insanlara tanıtırken ilk olarak kimliklerini oluşturtmak istedikleri ajansa haklarında tüm bilgileri eksiksiz bir şekilde verirler ve ajans firma hakkında bir araştırma yapar. Bunun sonrasında firmanın tasarımı için gerekli olan işlemleri yaparlar. Kurumsal Kimlik Çalışmalarında firmaların dikkat etmesi gereken ve insanların dikkatini çekmesi açısından kullandığı yöntemler var."
  },
  {
    title: "Ücretsiz Material Kişisel Blogger Teması",
    content: "Google’ın yeni tasarımı olan material tasarım ile hazırlanmış, SEO çalışmaları yapılmış, responsive blogger teması…"+
    "Piyasa çok az material tasarımlı blogger teması bulunuyor. Biz de bu sorunu kaldıralım dedik, material tasarımlı blogger teması geliştirdik. Üstelik bu temanın özellikleri saymakla bitmiyor;"+
    "Material Tasarımlı"+
    "SEO Uyumlu"+
    "Responsive"+
    "Hazır Safelink Oluşturucu"+
    "Material Yukarı Çık Butonu"+
    "Popüler Yazılar Eklentisi"+
    "Tamamen Türkçe (Zaten Türk Tasarım ☺)"
  },
  {
    title: "Yeni Mağazanız Ücretsiz PrestaShop Otomobil, Jant ve Lastikçi Teması.",
    content: "Özenli bir şekilde hazırlanmış web sitesi günümüzde modern tasarım trendleri ile çekiciliği sunmak ve kullanıcıların aklında unutulmaz bir izlenim bırakmak adına gerçekten muhteşem olacaktır.  Sanal ortam ile gerçek ortamın aslında bir farkı yoktur, sonuç itibari ile sanal ortamı yönetenler ve bu ortamın bir ortağı olan herkes, potansiyel bir müşteridir. Bu yüzden bir web sitesinin görünümü ve kullanım kolaylığı sizi rakipleriniz arasında fark edilebilir kılmalıdır. Aslında bu yazıda sizlere bahsetmek istediğim şey tam olarak ta bu, modern bir çevrim içi sağlam bir çevrimiçi mağaza!"
  },
  {
    title: "Profesyonel Web Sitesi Hazırlama İpuçları",
    content: "Günümüzde her işletmenin bir ihtiyacı olarak görülen web sitesi aynı zamanda internet kullanıcıları ile işletmeleri buluşturan bir satış ortamı haline gelmiştir. Profesyonel anlamda kalite açısından ön planda bir web sitesini hazırlamak için bilmeniz gereken ipuçlarını bu haberimizde ele alacağız. İnternet sitelerinin oluşumlarında birçok unsur bulunmasına rağmen şart koşulan 3 büyük unsur bulunmaktadır."
  },
  {
    title: "Tanıştırayım; İdemama",
    content: "Eğer siz de benim gibi iş hayatında son derece titiz iseniz işiniz zor! Kişi kendinden bilir işi sözüne sonuna kadar katılıyorum. Size hem başımdan geçen bir olayı anlatacağım hem de sizi bir site ile tanıştıracağım. Çoğunuz bu site nedeniyle bana teşekkür edecek, biliyorum. Aylar önce bir logoya ihtiyacım oldu. Birkaç firma ile görüştüm ve birkaç logo tasarımı örneği sunuldu. Ancak açıkçası hiç biri istediğim gibi değildi. Bir yandan da zamanım daralıyordu, artık logoyu bulup işlerimin devamını sağlamak zorundaydım. İnternette logo tasarımı yapan firma ararken karşıma idemamasitesi çıktı. Başladım siteyi incelemeye ve okuduklarımdan gayet hoşnut kaldım."
  },
  {
    title: "Fok Balıkları Neden Çok Yalnız?",
    content: "Fok balıkları yalnız takılırlar ve diğer türlerle fazla etkileşimde bulunmazlar bu yüzden yalnızlardır.Ayrıca suda kalabildikleri için suda yaşayabilirler.Yemek bulmak için de suda yaşarlar."
  },
  {
    title: "Profesyonel Web Sitesi Hazırlama İpuçları",
    content: "Günümüzde her işletmenin bir ihtiyacı olarak görülen web sitesi aynı zamanda internet kullanıcıları ile işletmeleri buluşturan bir satış ortamı haline gelmiştir. Profesyonel anlamda kalite açısından ön planda bir web sitesini hazırlamak için bilmeniz gereken ipuçlarını bu haberimizde ele alacağız. İnternet sitelerinin oluşumlarında birçok unsur bulunmasına rağmen şart koşulan 3 büyük unsur bulunmaktadır."
  },
  {
    title: "Tanıştırayım; İdemama",
    content: "Eğer siz de benim gibi iş hayatında son derece titiz iseniz işiniz zor! Kişi kendinden bilir işi sözüne sonuna kadar katılıyorum. Size hem başımdan geçen bir olayı anlatacağım hem de sizi bir site ile tanıştıracağım. Çoğunuz bu site nedeniyle bana teşekkür edecek, biliyorum. Aylar önce bir logoya ihtiyacım oldu. Birkaç firma ile görüştüm ve birkaç logo tasarımı örneği sunuldu."
  },
  {
    title: "Fok Balıkları Neden Çok Yalnız?",
    content: "Fok balıkları yalnız takılırlar ve diğer türlerle fazla etkileşimde bulunmazlar bu yüzden yalnızlardır.Ayrıca suda kalabildikleri için suda yaşayabilirler.Yemek bulmak için de suda yaşarlar."
  },
  {
    title: "Ücretsiz Material Kişisel Blogger Teması",
    content: "Google’ın yeni tasarımı olan material tasarım ile hazırlanmış, SEO çalışmaları yapılmış, responsive blogger teması…"
  },
  {
    title: "Yeni Mağazanız Ücretsiz PrestaShop Otomobil, Jant ve Lastikçi Teması.",
    content: "Özenli bir şekilde hazırlanmış web sitesi günümüzde modern tasarım trendleri ile çekiciliği sunmak ve kullanıcıların aklında unutulmaz bir izlenim bırakmak adına gerçekten muhteşem olacaktır.  Sanal ortam ile gerçek ortamın aslında bir farkı yoktur, sonuç itibari ile sanal ortamı yönetenler ve bu ortamın bir ortağı olan herkes, potansiyel bir müşteridir."
  },
  {
    title: "Profesyonel Web Sitesi Hazırlama İpuçları",
    content: "Günümüzde her işletmenin bir ihtiyacı olarak görülen web sitesi aynı zamanda internet kullanıcıları ile işletmeleri buluşturan bir satış ortamı haline gelmiştir. Profesyonel anlamda kalite açısından ön planda bir web sitesini hazırlamak için bilmeniz gereken ipuçlarını bu haberimizde ele alacağız. İnternet sitelerinin oluşumlarında birçok unsur bulunmasına rağmen şart koşulan 3 büyük unsur bulunmaktadır."
  },
  {
    title: "Kurumsal Kimlik Çalışması Nasıl Yapılır?",
    content: "Genel olarak çoğu insan tanınmak ve bilinmek ister. İnsanlar bunu isterken yaptıkları işin de tanınıp diğer herkesten ayrı yerlerde tutulmasının hayalini kurar. Peki sadece hayalde bırakmak bu düşünceyi ne kadar doğru? Eğer belirli bir yere gelememiş ve kendinizi duyuramamışsanız bunda sizin henüz bu iş için çalışmaya geçmemeniz etkilidir."
  },
  {
    title: "Ücretsiz Material Kişisel Blogger Teması",
    content: "Google’ın yeni tasarımı olan material tasarım ile hazırlanmış, SEO çalışmaları yapılmış, responsive blogger teması…"
  },
  {
    title: "Yeni Mağazanız Ücretsiz PrestaShop Otomobil, Jant ve Lastikçi Teması.",
    content: "Özenli bir şekilde hazırlanmış web sitesi günümüzde modern tasarım trendleri ile çekiciliği sunmak ve kullanıcıların aklında unutulmaz bir izlenim bırakmak adına gerçekten muhteşem olacaktır.  Sanal ortam ile gerçek ortamın aslında bir farkı yoktur, sonuç itibari ile sanal ortamı yönetenler ve bu ortamın bir ortağı olan herkes, potansiyel bir müşteridir. "
  },
  {
    title: "Profesyonel Web Sitesi Hazırlama İpuçları",
    content: "Günümüzde her işletmenin bir ihtiyacı olarak görülen web sitesi aynı zamanda internet kullanıcıları ile işletmeleri buluşturan bir satış ortamı haline gelmiştir. Profesyonel anlamda kalite açısından ön planda bir web sitesini hazırlamak için bilmeniz gereken ipuçlarını bu haberimizde ele alacağız. İnternet sitelerinin oluşumlarında birçok unsur bulunmasına rağmen şart koşulan 3 büyük unsur bulunmaktadır."
  },
  {
    title: "Tanıştırayım; İdemama",
    content: "Eğer siz de benim gibi iş hayatında son derece titiz iseniz işiniz zor! Kişi kendinden bilir işi sözüne sonuna kadar katılıyorum. Size hem başımdan geçen bir olayı anlatacağım hem de sizi bir site ile tanıştıracağım. Çoğunuz bu site nedeniyle bana teşekkür edecek, biliyorum. Aylar önce bir logoya ihtiyacım oldu. Birkaç firma ile görüştüm ve birkaç logo tasarımı örneği sunuldu. "
  },
  {
    title: "Fok Balıkları Neden Çok Yalnız?",
    content: "Fok balıkları yalnız takılırlar ve diğer türlerle fazla etkileşimde bulunmazlar bu yüzden yalnızlardır.Ayrıca suda kalabildikleri için suda yaşayabilirler.Yemek bulmak için de suda yaşarlar."
  },
  {
    title: "Profesyonel Web Sitesi Hazırlama İpuçları",
    content: "Günümüzde her işletmenin bir ihtiyacı olarak görülen web sitesi aynı zamanda internet kullanıcıları ile işletmeleri buluşturan bir satış ortamı haline gelmiştir. Profesyonel anlamda kalite açısından ön planda bir web sitesini hazırlamak için bilmeniz gereken ipuçlarını bu haberimizde ele alacağız. İnternet sitelerinin oluşumlarında birçok unsur bulunmasına rağmen şart koşulan 3 büyük unsur bulunmaktadır."
  },
  {
    title: "Tanıştırayım; İdemama",
    content: "Eğer siz de benim gibi iş hayatında son derece titiz iseniz işiniz zor! Kişi kendinden bilir işi sözüne sonuna kadar katılıyorum. Size hem başımdan geçen bir olayı anlatacağım hem de sizi bir site ile tanıştıracağım. Çoğunuz bu site nedeniyle bana teşekkür edecek, biliyorum. Aylar önce bir logoya ihtiyacım oldu. Birkaç firma ile görüştüm ve birkaç logo tasarımı örneği sunuldu. Ancak açıkçası hiç biri istediğim gibi değildi. "
  },
  {
    title: "Fok Balıkları Neden Çok Yalnız?",
    content: "Fok balıkları yalnız takılırlar ve diğer türlerle fazla etkileşimde bulunmazlar bu yüzden yalnızlardır.Ayrıca suda kalabildikleri için suda yaşayabilirler.Yemek bulmak için de suda yaşarlar."
  },
  {
    title: "Ücretsiz Material Kişisel Blogger Teması",
    content: "Google’ın yeni tasarımı olan material tasarım ile hazırlanmış, SEO çalışmaları yapılmış, responsive blogger teması…",
  },
  {
    title: "Yeni Mağazanız Ücretsiz PrestaShop Otomobil, Jant ve Lastikçi Teması.",
    content: "Özenli bir şekilde hazırlanmış web sitesi günümüzde modern tasarım trendleri ile çekiciliği sunmak ve kullanıcıların aklında unutulmaz bir izlenim bırakmak adına gerçekten muhteşem olacaktır.  Sanal ortam ile gerçek ortamın aslında bir farkı yoktur, sonuç itibari ile sanal ortamı yönetenler ve bu ortamın bir ortağı olan herkes, potansiyel bir müşteridir. "
  },
  {
    title: "Profesyonel Web Sitesi Hazırlama İpuçları",
    content: "Günümüzde her işletmenin bir ihtiyacı olarak görülen web sitesi aynı zamanda internet kullanıcıları ile işletmeleri buluşturan bir satış ortamı haline gelmiştir. Profesyonel anlamda kalite açısından ön planda bir web sitesini hazırlamak için bilmeniz gereken ipuçlarını bu haberimizde ele alacağız. İnternet sitelerinin oluşumlarında birçok unsur bulunmasına rağmen şart koşulan 3 büyük unsur bulunmaktadır."
  },
  {
    title: "Profesyonel Web Sitesi Hazırlama İpuçları",
    content: "Günümüzde her işletmenin bir ihtiyacı olarak görülen web sitesi aynı zamanda internet kullanıcıları ile işletmeleri buluşturan bir satış ortamı haline gelmiştir. Profesyonel anlamda kalite açısından ön planda bir web sitesini hazırlamak için bilmeniz gereken ipuçlarını bu haberimizde ele alacağız. İnternet sitelerinin oluşumlarında birçok unsur bulunmasına rağmen şart koşulan 3 büyük unsur bulunmaktadır."
  },
  {
    title: "Profesyonel Web Sitesi Hazırlama İpuçları",
    content: "Günümüzde her işletmenin bir ihtiyacı olarak görülen web sitesi aynı zamanda internet kullanıcıları ile işletmeleri buluşturan bir satış ortamı haline gelmiştir. Profesyonel anlamda kalite açısından ön planda bir web sitesini hazırlamak için bilmeniz gereken ipuçlarını bu haberimizde ele alacağız. İnternet sitelerinin oluşumlarında birçok unsur bulunmasına rağmen şart koşulan 3 büyük unsur bulunmaktadır."
  },
];

class Contents extends Component {
    constructor(props) {
        super(props);
        this.state = {
          page: 0,
          modalShow: false
        };
    }

    // ilgili kartı ekrana döndüren metot
    getColumn(data){
      return(
        <Card 
          data = {data}
          moreAction = {()=>{ 
            this.handleModalShow(data);
          }}
        />
      )
    }

    // kart tasarımlarını çağıran metot
    getCardField(){
      let page = this.state.page;
      let content = blogList.slice(page*6, (page*6 + 6));
      return(
        <div className="container">
          <Row className="show-grid">
            {content[0] ? this.getColumn(content[0]) : null}
            {content[1] ? this.getColumn(content[1]) : null}
            {content[2] ? this.getColumn(content[2]) : null}
            {content[3] ? this.getColumn(content[3]) : null}
            {content[4] ? this.getColumn(content[4]) : null}
            {content[5] ? this.getColumn(content[5]) : null}
          </Row>
        </div>
      )
    }

    // blog postları için sayfalama yapar
    getPager(){
      return(
        <div className="container">
          <Pager style = {{}}>
            <Pager.Item previous 
              onClick = {()=>{
                if (this.state.page > 0) {
                  this.setState({ page : this.state.page - 1 });
                }
              }}>
              &larr; Geri
            </Pager.Item>
            <Pager.Item next onClick = {()=>{
                if (blogList.length > ((this.state.page+1)*6)) {
                  this.setState({ page : this.state.page + 1 });
                }
              }}>
              İleri &rarr;
            </Pager.Item>
          </Pager>
        </div>
      )
    }

    handleModalShow(data){
      this.setState({
        modalShow: true,
        selectedContent: data
      });
    }

    handleModalHide(){
      this.setState({
        modalShow: false
      });
    }

    getModal(){
      return(
        <div className="static-modal">
          <Modal
            show={this.state.modalShow}
            onHide={this.handleHide}
            container={this}
            bsSize="large"
            aria-labelledby="contained-modal-title-lg">
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-lg">
                {this.state.selectedContent['title']}
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <p style = {{fontSize: 16}}>{this.state.selectedContent['content']}</p>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={()=>{this.handleModalHide()}}>Kapat</Button>
            </Modal.Footer>
          </Modal>
        </div>
      )
    }

    render() {
      return (
        <div className="Content" style={{padding:10}}>
          <h1 className ="text-center strong" style = {{color: '#E7E7E7'}}>BLOG</h1>
          {this.getCardField()}
          {this.getPager()}
          {this.state.modalShow ? this.getModal() : null}
        </div>
      );
    }
}

class Card extends Component {
  constructor(props) {
      super(props);
      this.state = {
        hover: false
      };
  }


  render(){
    return(
      <Col xs={12} sm={6} md={4}>
        <div 
          onMouseEnter={()=>{ 
            this.setState({ hover: true });
          }}
          onMouseLeave={()=>{
            this.setState({ hover: false });
          }}
          style={{
            backgroundColor: this.state.hover ? '#FFFFFF' : '#E7E7E7',
            borderRadius: 4
          }}>
          <div style={styles.cardStyle}>
          <div style={styles.cardTitleStyle}>
            <a onClick={()=>{ this.props.moreAction(); }} style = {{textDecoration: 'none'}}>
              <h3>{this.props.data.title}</h3>
            </a>
          </div>

          <div style={styles.cardInnerStyle}>
            <p>{this.props.data.content}</p>
          </div>
          
          <div style={styles.cardMoreField}>
            <Button 
              bsStyle="primary" 
              bsSize="xsmall" 
              onClick={()=>{
                this.props.moreAction();
              }}>
              Devamını oku..
            </Button>
          </div>
        </div>
        </div>
      </Col>
    )
  }
}

let styles = {
  cardStyle: {
    height: window.innerHeight / 3,
    backgroundColor: 'transparent',
    marginBottom: 15,
    marginTop: 15,
    padding: 5,
    borderRadius: 4,
    display: 'flex',
    flexDirection: 'column',
  },
  cardInnerStyle: {
    flex: 8,
    overflow: 'hidden',
  },
  cardTitleStyle: {
    flexGrow: 1,
  },
  cardMoreField: {
    marginTop: 4
  }
}

export default Contents;
