/*
** Layout yapısını oluşturduğumuz sınıf
*/

import React, {Component} from 'react';
import Header from './Header';
import Welcome from './Welcome';
import Footer from './Footer';
import Contents from './Contents';
import Contact from './Contact';
import { BrowserRouter, Route } from 'react-router-dom';
var data = require('./../store/data');

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <BrowserRouter>
                <div className="Home" style={{}}>
                    <Route 
                        path='/'
                        render={ props => <Header {...props} />}
                    />

                    <div id="welcome" style={{backgroundColor: '#F1EBE0', minHeight: window.innerHeight}}>
                        <Route path='/' component={Welcome} />
                    </div>

                    <div id="contents" style={{backgroundColor: '#373A47', minHeight: (window.innerHeight - (data.navbarHeight || 50))}}>
                        <Route path='/' component={Contents} />
                    </div>

                    <div id="iletisim" style={{display:"flex", minHeight: window.innerHeight - (data.navbarHeight || 50), justifyContent:"center", alignItems:"center" }}>
                        <Route path='/' component={Contact} />
                    </div>

                    <div style={{backgroundColor:'#E7E7E7'}}>
                        <Route path='/' component={Footer} />
                    </div>
                </div>
            </BrowserRouter>
        );
    }
}

export default Home;
