/*
** İletişim kısmı
*/

import React, {Component} from 'react';
import './../App.css';
import {
    Panel,
    InputGroup,
    Button,
    Col,
    ControlLabel,
    FormControl,
    FormGroup,
    Glyphicon,
    HelpBlock,
    Image
} from "react-bootstrap";

class Contact extends Component {
    constructor(props, context) {
        super(props, context);

        this.handleChange = this.handleChange.bind(this);

        this.state = {
            value: ''
        };
    }

    getValidationState() {
        const length = this.state.value.length;
        if (length > 10) return 'success';
        else if (length > 5) return 'warning';
        else if (length > 0) return 'error';
        return null;
    }

    handleChange(e) {
        this.setState({value: e.target.value});
    }

    render() {
        return (
            <form style={{padding: 20}} horizontal="true">
                <Panel  style={{display: "flex", flexDirection: "row" ,padding:30}}>
                    <FormGroup
                        controlId="formContact" style={{flex: 1,justifyContent:"center"}}
                        xs={4} md={4}>
                        <Image  src={require('./../img/emailContact.gif')} style={{resizeMode: "contain"}} responsive/>
                    </FormGroup>

                    <FormGroup
                        controlId="formContact"
                        validationState={this.getValidationState()}
                        style={{padding: 20, flex: 3}}
                        xs={8} md={8}>
                        <Col componentClass={ControlLabel} sm={2} >
                            <ControlLabel> E-Mail :</ControlLabel>
                        </Col>
                        <Col sm={10}>
                            <InputGroup>
                                <InputGroup.Addon><Glyphicon glyph="glyphicon glyphicon-user"/></InputGroup.Addon>
                                <FormControl
                                    type="email"
                                    value={this.state.value}
                                    placeholder="xxxxx@xx.com"
                                    onChange={this.handleChange}
                                />
                            </InputGroup>
                            <FormControl.Feedback/>
                            <HelpBlock>yazılacak uyarılar</HelpBlock>
                        </Col>

                        <Col componentClass={ControlLabel} sm={2}>
                            <ControlLabel>Konu :</ControlLabel>
                        </Col>
                        <Col sm={10}>
                            <InputGroup>
                                <InputGroup.Addon><Glyphicon glyph="glyphicon glyphicon-tag"/></InputGroup.Addon>
                                <FormControl
                                    type="text"
                                    value={this.state.value}
                                    placeholder="Enter text"
                                    onChange={this.handleChange}
                                />
                            </InputGroup>
                            <FormControl.Feedback/>
                            <HelpBlock>yazılacak uyarılar</HelpBlock>
                        </Col>
                        <Col componentClass={ControlLabel} sm={2}>

                            <ControlLabel>Mesaj :</ControlLabel>
                        </Col>
                        <Col sm={10}>
                            <InputGroup>
                                <InputGroup.Addon><Glyphicon glyph="glyphicon glyphicon-pencil"/></InputGroup.Addon>
                                <FormControl componentClass="textarea" placeholder="textarea" rows="10"/>
                            </InputGroup>
                        </Col>
                        <Col sm={2}>
                        </Col>
                        <Col sm={10}>
                            <Button type="submit" className="btn btn-primary"  block style={{marginTop:10}}>Gönder</Button>
                        </Col>
                    </FormGroup>
                </Panel>
            </form>
        );
    }
}


export default Contact;
