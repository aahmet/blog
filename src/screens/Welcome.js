/*
** Giriş sayfasında slider bulunan alan 
*/

import React, {Component} from 'react';
import { Carousel } from 'react-bootstrap';
var data = require('./../store/data');

class Welcome extends Component {
    constructor(props) {
        super(props);
        this.state = {
            navHeight: data.navbarHeight ? data.navbarHeight :  51
        };

        // bind methods
        this.updateDimensions = this.updateDimensions.bind(this);
    }

    componentWillMount() {
        window.addEventListener("resize", this.updateDimensions);
    }

    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }

    updateDimensions(){
        this.setNavbarHeight();    
    }

    setNavbarHeight(){
        this.setState({ 
            navHeight: data.navbarHeight ? data.navbarHeight :  51
        });
    }

    render() {
        return (
            <div 
              className="Welcome"
              style={{
                paddingTop: this.state.navHeight+50,
                paddingBottom: 50,
                display: 'flex', 
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                }}>
                <Carousel style = {{maxWidth: 900, maxHeight: 500}}>
                    <Carousel.Item>
                        <img width={900} height={500} alt="900x500" src={require('./../img/slider1.jpg')}/>
                        <Carousel.Caption>
                            <h3>Blog Projesi</h3>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img width={900} height={500} alt="900x500" src={require('./../img/slider2.jpg')} />
                        <Carousel.Caption>
                            <h3>%100 Open Source</h3>
                            <p></p>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img width={900} height={500} alt="900x500" src={require('./../img/slider3.png')} />
                        <Carousel.Caption>
                            <h3>Parlak fikirleri daima takip et.</h3>
                        </Carousel.Caption>
                    </Carousel.Item>
                </Carousel>
            </div>
        );
    }
}

export default Welcome;
