/*
** Anlık verilerin props ile taşıma yerine session cache üzerinde tutar
*/

'use-strict'

import {observable, action} from 'mobx';
var data = observable({

    //Screen Size
    navbarHeight: 50,
    innerWidth: 800,
    innerHeight: 600,

    // action: verilere default değerleri tekrar atamak istersek kullanabiliriz
    setLogoutDefaultObject: action(function() {
        this.navbarHeight = 50;
        this.innerWidth = 800;
        this.navbarHeight = 600;
    }),
});

export default data;
